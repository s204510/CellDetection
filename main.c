#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "cbmp.h"

#define threshold 100
#define detectionArea 16
#define borderSize 1
#define exclusionFrame detectionArea + (borderSize * 2)
#define whitesInExclusion 0
#define crossSize 15
#define CHAR_BIT 8
#define dataArraySize (BMP_WIDTH * BMP_HEIGTH / CHAR_BIT) + 1
#define debug 1
#define numOfChars 12
#define charArrayLength (300 + 10) * numOfChars + 1
#define printCords 0
#define calculateThreshold 1

struct cell_coordinates {
    unsigned short x;
    unsigned short y;
};

void RGB_to_gray(unsigned char[BMP_HEIGTH][BMP_WIDTH][3], unsigned char[BMP_HEIGTH][BMP_WIDTH]);
void convert_to_binary(unsigned char[BMP_WIDTH][BMP_HEIGTH], unsigned char[dataArraySize], unsigned short t);
char erode(unsigned char[dataArraySize]);
void detect_cells(unsigned char[dataArraySize]);
void mark_cells(unsigned char[BMP_HEIGTH][BMP_WIDTH][BMP_CHANNELS], unsigned char[BMP_HEIGTH][BMP_WIDTH]);
void set_bit(unsigned char[dataArraySize], int);
void clear_bit(unsigned char[dataArraySize], int);

#if printCords
void add_cell_to_out(struct cell_coordinates);
char outPutSring[charArrayLength];
#endif

unsigned char input_image[BMP_HEIGTH][BMP_WIDTH][BMP_CHANNELS];

#if calculateThreshold
unsigned int histogram[256];

void createHistogram(unsigned char in[BMP_HEIGTH][BMP_WIDTH]) {
    for (short i = 0; i < BMP_HEIGTH; i++) {
        for (short j = 0; j < BMP_WIDTH; j++) {
            histogram[in[i][j]]++;
        }
    }
}

short calcOtsu() {
    unsigned int total = BMP_WIDTH * BMP_HEIGTH;
    unsigned short top = 256;
    unsigned int sumB = 0;
    double wB = 0;
    double maximum = 0.0;
    unsigned char level = 0;  //???
    unsigned int sum1 = 0;
    for (int i = 0; i < 256; i++) {
        sum1 += histogram[i] * (i);
    }

    for (int i = 0; i < 256; i++) {
        unsigned int wF = total - wB;
        if (wB > 0 && wF > 0) {
            double mF = (double)(sum1 - sumB) / wF;
            double val = wB * wF * ((sumB / wB) - mF) * ((sumB / wB) - mF);
            if (val >= maximum) {
                level = i + 1;
                maximum = val;
            }
        }
        wB += histogram[i];
        sumB = sumB + i * histogram[i];
    }

    return level - (level - 90) * 0.6;
}
#endif

int coord_to_index(short x, short y) {
    return y * BMP_WIDTH + x;
}

#if debug
unsigned char debug_image[BMP_HEIGTH][BMP_WIDTH][BMP_CHANNELS];

void binary_to_out(unsigned char pic[BMP_HEIGTH][BMP_WIDTH][3], unsigned char binary[dataArraySize]) {
    for (short i = 0; i < BMP_HEIGTH; i++) {
        for (short j = 0; j < BMP_WIDTH; j++) {
            int bitIndex = coord_to_index(i, j);

            if ((binary[bitIndex / CHAR_BIT] & (1 << bitIndex % CHAR_BIT)) != 0) {
                pic[i][j][0] = 255;
                pic[i][j][1] = 255;
                pic[i][j][2] = 255;
            } else {
                pic[i][j][0] = 0;
                pic[i][j][1] = 0;
                pic[i][j][2] = 0;
            }
        }
    }
}
#endif

unsigned int cellsFound = 0;

int main(int argc, char** argv) {
    double execution_time = 0.0;
    clock_t start_time = clock();  // For measuring execution time.

    if (argc != 3) {
        fprintf(stderr, "Usage: %s <output file path> <output file path>\n", argv[0]);
        exit(1);
    }

    read_bitmap(argv[1], input_image);
    unsigned char greyImage[BMP_HEIGTH][BMP_WIDTH];
    RGB_to_gray(input_image, greyImage);
    unsigned char bin_data[dataArraySize];

#if calculateThreshold
    createHistogram(greyImage);
    unsigned char calT = calcOtsu();
#if debug
    printf("Using calculatet threshold: %d\n", calT);
#endif
    convert_to_binary(greyImage, bin_data, calT);
#else
    convert_to_binary(greyImage, bin_data, threshold);
#endif

#if debug
    binary_to_out(debug_image, bin_data);
    write_bitmap(debug_image, "debugImages/before.bmp");

    for (int i = 0; i < 1000; i++) {
        int returnedValue = erode(bin_data);
        if (!returnedValue) {
            break;
        } else {
            char str[50];
            char str2[50];
            sprintf(str, "debugImages/outputBlackAndWhite %d.bmp", i);
            sprintf(str2, "debugImages/outputFound %d.bmp", i);
            binary_to_out(debug_image, bin_data);
            write_bitmap(debug_image, str);
            detect_cells(bin_data);
            printf("Completed run %d with total cells found %d\n\n\n", i, cellsFound);
            write_bitmap(input_image, str2);
        }
    }
#else
    int returnedValue = 1;
    while (returnedValue) {
        returnedValue = erode(bin_data);
        detect_cells(bin_data);
    }
#endif
    write_bitmap(input_image, argv[2]);

#if printCords
    outPutSring[charArrayLength - 1] = '\0';
    printf("%.*s \n", charArrayLength, outPutSring);
#endif
    printf("Program ran successfully and found %d cells\n", cellsFound);

    clock_t stop_time = clock();
    execution_time = (double)(stop_time - start_time) / CLOCKS_PER_SEC;
    printf("The execution time was %f seconds.", execution_time);

    return 0;
}

void RGB_to_gray(unsigned char pic[BMP_HEIGTH][BMP_WIDTH][BMP_CHANNELS], unsigned char greyPic[BMP_HEIGTH][BMP_WIDTH]) {
    for (short i = 0; i < BMP_HEIGTH; i++) {
        for (short j = 0; j < BMP_WIDTH; j++) {
            greyPic[i][j] = (pic[i][j][0] + pic[i][j][1] + pic[i][j][2]) / 3;
        }
    }
}

void convert_to_binary(unsigned char grey[BMP_WIDTH][BMP_HEIGTH], unsigned char bin_data[dataArraySize], unsigned short t) {  // Reads input array by rows thereafter by columns
    for (short x = 0; x < BMP_WIDTH; x++) {
        for (short y = 0; y < BMP_HEIGTH; y++) {
            int bitIndexToSet;
            bitIndexToSet = coord_to_index(x, y);

            if (grey[x][y] > t) {
                set_bit(bin_data, bitIndexToSet);
            } else {
                clear_bit(bin_data, bitIndexToSet);
            }
        }
    }
}

struct cell_coordinates index_to_coord(int index) {
    struct cell_coordinates cord;
    cord.x = index / CHAR_BIT;
    cord.y = index % CHAR_BIT;
    return cord;
}

void set_bit(unsigned char* source, int n) {  // Set the n'th bit in source
    source[n / CHAR_BIT] |= 1 << (n % CHAR_BIT);
}

void clear_bit(unsigned char source[], int n) {  // Clear the n'th bitbit in source
    source[n / CHAR_BIT] &= ~(1 << (n % CHAR_BIT));
}

char erode(unsigned char binaryImage[dataArraySize]) {
    char returnVal = 0;
    unsigned char outImage[dataArraySize];

    for (short i = 0; i < BMP_HEIGTH; i++) {
        for (short j = 0; j < BMP_WIDTH; j++) {
            char needToFind = 4 - (i == 0 + j == 0 + i == BMP_HEIGTH - 1 + j == BMP_WIDTH - 1);
            char whitesFound = 0;
            int bitIndex;

            bitIndex = coord_to_index(i - 1, j);
            if (i > 0 && (binaryImage[bitIndex / CHAR_BIT] & (1 << bitIndex % CHAR_BIT)) != 0) {
                whitesFound++;
            }
            bitIndex = coord_to_index(i, j - 1);
            if (j > 0 && (binaryImage[bitIndex / CHAR_BIT] & (1 << bitIndex % CHAR_BIT)) != 0) {
                whitesFound++;
            }
            bitIndex = coord_to_index(i + 1, j);
            if (i < BMP_HEIGTH - 1 && (binaryImage[bitIndex / CHAR_BIT] & (1 << bitIndex % CHAR_BIT)) != 0) {
                whitesFound++;
            }
            bitIndex = coord_to_index(i, j + 1);
            if (j < BMP_WIDTH - 1 && (binaryImage[bitIndex / CHAR_BIT] & (1 << bitIndex % CHAR_BIT)) != 0) {
                whitesFound++;
            }
            bitIndex = coord_to_index(i, j);
            if (whitesFound < needToFind) {
                clear_bit(outImage, bitIndex);
            } else {
                set_bit(outImage, bitIndex);
                returnVal = 1;  //Burde registrere om vi fjerner noget, men lige nu registrere den bare om der er noget der er omgivet at hvidt.
            }
        }
    }
    memcpy(binaryImage, outImage, dataArraySize);
    return returnVal;
}

void createCrossAt(struct cell_coordinates cord) {
    for (int i = 0; i < crossSize; i++) {
        input_image[cord.x + i][cord.y + crossSize / 2][0] = 255;
        input_image[cord.x + i][cord.y + crossSize / 2][1] = 0;
        input_image[cord.x + i][cord.y + crossSize / 2][2] = 0;
    }
    for (int i = 0; i < crossSize; i++) {
        input_image[cord.x + crossSize / 2][cord.y + i][0] = 255;
        input_image[cord.x + crossSize / 2][cord.y + i][1] = 0;
        input_image[cord.x + crossSize / 2][cord.y + i][2] = 0;
    }
}

void detect_cells(unsigned char binary[dataArraySize]) {
    int bitIndex;

    for (short i = 0; i <= BMP_HEIGTH - detectionArea; i++) {
        for (short j = 0; j <= BMP_WIDTH - detectionArea; j++) {
            char whiteFoundIn = 0;
            for (short k = 0; k < detectionArea; k++) {
                for (short l = 0; l < detectionArea; l++) {
                    bitIndex = coord_to_index(i + k, j + l);
                    if ((binary[bitIndex / CHAR_BIT] & (1 << bitIndex % CHAR_BIT)) != 0) {
                        whiteFoundIn = 1;
                        break;
                    }
                }
                if (whiteFoundIn) {
                    break;
                }
            }

            if (whiteFoundIn) {
                unsigned char whiteFoundOut = 0;

                short minI = i == 0 ? 0 : i - borderSize;
                short minJ = j == 0 ? 0 : j - borderSize;
                short maxI = i == (BMP_HEIGTH - detectionArea) ? BMP_HEIGTH : i + detectionArea;  // Bør kigges RIGTIG godt igennem her - tror fejlen ligger her.
                short maxJ = j == (BMP_WIDTH - detectionArea) ? BMP_WIDTH : j + detectionArea;

                //printf("Vi kigger på en firkant fra %d:%d \t til %d:%d \t med i/j værdigerne %d,%d\n",minI,minJ,maxI,maxJ,i,j);

                if (j != 0) {
                    for (int k = minI; k < maxI; k++) {
                        bitIndex = coord_to_index(k, minJ);
                        if ((binary[bitIndex / CHAR_BIT] & (1 << bitIndex % CHAR_BIT)) != 0) {
                            whiteFoundOut++;
                        }
                    }
                }
                if (i != 0) {
                    for (int k = minJ; k < maxJ; k++) {
                        bitIndex = coord_to_index(minI, k);
                        if ((binary[bitIndex / CHAR_BIT] & (1 << bitIndex % CHAR_BIT)) != 0) {
                            whiteFoundOut++;
                        }
                    }
                }
                if (j < (BMP_WIDTH - detectionArea)) {
                    for (int k = minI; k < maxI; k++) {
                        bitIndex = coord_to_index(k, maxJ);
                        if ((binary[bitIndex / CHAR_BIT] & (1 << bitIndex % CHAR_BIT)) != 0) {
                            whiteFoundOut++;
                        }
                    }
                }
                if (i < (BMP_HEIGTH - detectionArea)) {
                    for (int k = minJ; k < maxJ; k++) {
                        bitIndex = coord_to_index(maxI, k);
                        if ((binary[bitIndex / CHAR_BIT] & (1 << bitIndex % CHAR_BIT)) != 0) {
                            whiteFoundOut++;
                        }
                    }
                }

                if (whiteFoundOut <= whitesInExclusion) {
                    struct cell_coordinates cord;
                    cord.x = i;
                    cord.y = j;
#if printCords
                    add_cell_to_out(cord);
#endif
                    cellsFound++;
                    //printf("Cell found at %d:%d\n",cord.x,cord.y);
                    createCrossAt(cord);

                    for (short k = minI; k < maxI; k++) {
                        for (short l = minJ; l < maxJ; l++) {
                            bitIndex = coord_to_index(k, l);
                            clear_bit(binary, bitIndex);
                            //printf("reset cell %d:%d\t den har nu værdigen %d \t gammel: %d\n",k,l, binary[k][l],prev);
                        }
                    }
                }
            } else if (j < BMP_WIDTH - detectionArea * 2) {
                j += detectionArea - 1;
            }
        }
    }
}

#if printCords
void add_cell_to_out(struct cell_coordinates in) {
    char tempX[3];
    char tempY[3];
    sprintf(tempX, "%d", in.x);
    sprintf(tempY, "%d", in.y);

    outPutSring[cellsFound * numOfChars] = 'X';
    outPutSring[cellsFound * numOfChars + 1] = ':';
    outPutSring[cellsFound * numOfChars + 2] = tempX[0] == 0 ? 48 : tempX[0];
    outPutSring[cellsFound * numOfChars + 3] = tempX[1] == 0 ? 48 : tempX[1];
    outPutSring[cellsFound * numOfChars + 4] = tempX[2] == 0 ? 48 : tempX[2];
    outPutSring[cellsFound * numOfChars + 5] = '\t';
    outPutSring[cellsFound * numOfChars + 6] = 'Y';
    outPutSring[cellsFound * numOfChars + 7] = ':';
    outPutSring[cellsFound * numOfChars + 8] = tempY[0] == 0 ? 48 : tempY[0];
    outPutSring[cellsFound * numOfChars + 9] = tempY[1] == 0 ? 48 : tempY[1];
    outPutSring[cellsFound * numOfChars + 10] = tempY[2] == 0 ? 48 : tempY[2];
    outPutSring[cellsFound * numOfChars + 11] = '\n';
}
#endif
